import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Headers, Http, Response, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

declare var bcrypt: any;

@Injectable({
  providedIn: 'root'
})

export class DataService {
    private headers: Headers = new Headers;

    constructor(private _http: Http, private router: Router) {}

    public getProducts() {

        const salt = bcrypt.genSaltSync(10);
        const token = bcrypt.hashSync('Sx3q1mhyH65', salt);

        const headers = new Headers({
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': token });

        const options = new RequestOptions({ headers : headers });

        return new Promise<any>((resolve, reject) => {
            this._http.get('http://api.ciklum.test/api/users', options)
                .pipe(map((res: Response) => res.json()))
                .subscribe(
                    (res) => {
                        resolve(res);
                    },
                    (error) => {
                        reject(error);
                    }
                );
        });
    }

}
