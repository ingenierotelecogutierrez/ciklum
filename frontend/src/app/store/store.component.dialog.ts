import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'store-component-dialog',
    templateUrl: 'store.component.dialog.html',
  })
  export class StoreComponentDialog {
  
    constructor(
      public dialogRef: MatDialogRef<StoreComponentDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any) {}
  
    onNoClick(): void {
      this.dialogRef.close(
        {"accepted" : false}
      );
    }

    onYesClick(data): void {
        this.dialogRef.close(
          {
              "accepted" : true,
              "element" : data
        }
        );
      }
  
  }