import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { StoreComponentDialog } from './store.component.dialog';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})

export class StoreComponent implements OnInit {

  constructor(
    private dataservice: DataService,
    public dialog: MatDialog
    ) {}

  displayedColumns: string[] = ['id', 'name', 'description', 'price', 'stock', 'actions'];  
  products;

  dataSource = new MatTableDataSource<any>();

  ngOnInit() {
    this.dataservice.getProducts().then( (data) => {
      this.dataSource = new MatTableDataSource<any>(data);
    });

  }

  addToBasket(element){
    console.log(element);

    const dialogRef = this.dialog.open(StoreComponentDialog, {
      width: '250px',
      data: {
        element: element
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
    });
    
  }

}
