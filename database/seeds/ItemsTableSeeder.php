<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Truncate
        Item::truncate();

        $item = new Item();
        $item->id = 1;
        $item->name = 'Angular Ready';
        $item->description = 'book about Angular';
        $item->price = 20.20;
        $item->stock = 100;
        $item->save();

        $item = new Item();
        $item->id = 2;
        $item->name = 'TypeScript in Action';
        $item->description = 'book about TypeScript';
        $item->price = 20.20;
        $item->stock = 100;
        $item->save();

        $item = new Item();
        $item->id = 3;
        $item->name = 'Asp.net Core jump start';
        $item->description = 'book about asp.net core';
        $item->price = 20.20;
        $item->stock = 100;
        $item->save();

        $item = new Item();
        $item->id = 4;
        $item->name = 'Docker for dummies';
        $item->description = 'Book about docker';
        $item->price = 20.20;
        $item->stock = 100;
        $item->save();

    }
}
