<?php

namespace App\Http\Middleware;

use Closure;
use Hash;
use Illuminate\Http\Response;

class PublicAuth
{

    /**
     * The hash to make an token for public access.
     *
     * @var string
     */
    private $hash = 'Sx3q1mhyH65';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(
            $request->headers->has("authorization")
            &&
            Hash::check($this->hash, $request->headers->get("authorization"))
        ){

            return $next($request);

        }

        return response()->json(['message' => "The request header did not contain the necessary authentication codes, and the client is denied access."], Response::HTTP_UNAUTHORIZED);

    }
}
