<?php

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Item;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller {

    public function index () {

        try {

            $items = Item::all();

            if ($items) {

                return response()->json($items, Response::HTTP_OK);

            }

            return response()->json(['message' => 'stock out',], Response::HTTP_NO_CONTENT);

        } catch (\Exception $e) {

            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }

    }

}
